function hello() {
  console.log('Hello from JS Module');
}

function sup() {
  console.log('Sup?');
}

export {hello, sup};
