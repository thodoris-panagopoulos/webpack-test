const path = require('path');

const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const TerserJSPlugin = require('terser-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const {CleanWebpackPlugin} = require('clean-webpack-plugin');
const BrowserSyncPlugin = require('browser-sync-webpack-plugin');

module.exports = {
  entry: {
    app:'./src/index.js',
    secondary: './src/secondary.js'
  },
  output: {
    filename: '[name].bundle.js',
    path: path.resolve(__dirname, 'dist')
  },
  devtool: 'inline-source-map',
  devServer: {
    contentBase: './dist'
  },
  optimization: {
    minimizer: [new TerserJSPlugin({}), new OptimizeCSSAssetsPlugin({})],
  },
  module: {
    rules: [
      {
        test: /\.css$/,
        use: [
          {
            loader: MiniCssExtractPlugin.loader,
            options: {
              // only enable hot in development
              hmr: process.env.NODE_ENV === 'development',
              // if hmr does not work, this is a forceful method.
              reloadAll: true,
            },
          },
          //'style-loader',
          {
            loader: 'css-loader',
            options: {sourceMap: true}
          }
        ]
      },
      {
        test: /\.scss$/,
        use: [
          {loader: 'style-loader'},
          {loader: 'css-loader'},
          {loader: 'sass-loader'}
        ]
      },
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: [
          {loader: 'babel-loader'}
        ]
      }

    ]
  },
  plugins: [
      new CleanWebpackPlugin(),
      new MiniCssExtractPlugin({
        // Options similar to the same options in webpackOptions.output
        // both options are optional
        filename: '[name].css',
        chunkFilename: '[id].css',
      }),
      new HtmlWebpackPlugin({
        filename: 'index.html',
        template: 'src/views/index.html',
        chunks: ['app']
      }),
      new HtmlWebpackPlugin({
        filename: 'secondary.html',
        template: 'src/views/secondary.html',
        chunks: ['secondary']
      }),
      new BrowserSyncPlugin(
     // BrowserSync options
     {
       // browse to http://localhost:3000/ during development
       host: 'localhost',
       port: 3000,
       // proxy the Webpack Dev Server endpoint
       // (which should be serving on http://localhost:3100/)
       // through BrowserSync
       proxy: 'http://localhost:8081/'
     },
     // plugin options
     {
       // prevent BrowserSync from reloading the page
       // and let Webpack Dev Server take care of this
       reload: false
     }
   )
    ]
};
